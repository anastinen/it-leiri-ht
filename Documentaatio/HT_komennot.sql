CREATE TABLE "Machine" (
	"MachineID"			INTEGER			PRIMARY KEY NOT NULL,
	"Code"				VARCHAR(32),
	"City"				VARCHAR(32),
	"Address"			VARCHAR(64),
	"Postoffice"		VARCHAR(64),
	"Availability" 		VARCHAR(64),
	"Latitude"			VARCHAR(64),
	"Longitude"			VARCHAR(64)
);


CREATE TABLE "Package" (
	"PackageID"			INTEGER			PRIMARY KEY NOT NULL,
	"MachineID"			INTEGER,
	"DestinationID"		INTEGER,
	"Class"				INTEGER,
	"Breaks"			BOOL,
	"Distance"			INTEGER,			

	FOREIGN KEY("MachineID") REFERENCES "Machine"("MachineID") ON DELETE CASCADE,
	FOREIGN KEY("DestinationID") REFERENCES "Machine"("MachineID"),

	CHECK 	((Class = 1 AND Distance <= 150) 	OR
	      	(Class = 2 AND Distance <= 1500)	OR
		 	(Class = 3 AND Distance <= 1500))
);

CREATE TABLE "Item" (
	"Name"				VARCHAR(32)		PRIMARY KEY NOT NULL,
	"Weight"			INTEGER,
	"Length"			INTEGER,
	"Width"				INTEGER,
	"Height"			INTEGER,
	"Fragile"			BOOL
);

CREATE TABLE "ItemInPackage" (
	"ItemID"			INTEGER			PRIMARY KEY NOT NULL,
	"PackageID"			INTEGER,
	"ItemType"			VARCHAR(64),
	"Broken"			BOOL			NOT NULL DEFAULT 0,

	/*FOREIGN KEY("ItemType") REFERENCES "Item"("Name"),*/
	FOREIGN KEY("PackageID") REFERENCES "Package"("PackageID") ON DELETE CASCADE
);

INSERT INTO Item (Name, Weight, Length, Width, Height, Fragile) VALUES ("Pallo", 1, 20, 20, 20, 1);
INSERT INTO Item (Name, Weight, Length, Width, Height, Fragile) VALUES ("Sohva", 200, 350, 100, 80, 0);
INSERT INTO Item (Name, Weight, Length, Width, Height, Fragile) VALUES ("Alasin", 80, 50, 15, 40, 0);
INSERT INTO Item (Name, Weight, Length, Width, Height, Fragile) VALUES ("Maljakko", 1, 10, 10, 30, 1);
