package OlioHT;

/**
 * CT60A2411 Olio-ohjelmointi kesäkurssi/CT60A4303 Tietokantojen perusteet kesäkurssi 2018
 * Package.java
 * @author Antti Vilkman 0521281
 * 
 * This class handles the information of individual packages
 */

public class Package {
    protected int packageID;
    protected int machineID;
    protected int destinationID;
    protected int clas;
    protected String location;
    protected boolean breaks;
    protected Item contained;
    protected boolean isContentBroken = false;
    
    public Package(int pID, int mID, int dID, int c, boolean brks, String loc, Item i) {
        packageID = pID;
        machineID = mID;
        destinationID = dID;
        location = loc;
        clas = c;
        breaks = brks;
        contained = i;
    }
    
    @Override
    public String toString() {
        return "Luokka " + clas + ", " + location + ", " + contained.getName();
    }
    
    public int getID() {
        return packageID;
    }
    
    public int getMacID() {
        return machineID;
    }   
    
    public int getDestID() {
        return destinationID;
    }
    
    public int getClas() {
        return clas;
    }
    
    public void send(String newLoc) {
        machineID = destinationID;
        location = newLoc;
        if (this.breaks) {
            isContentBroken = true;
        }
    }
    
    // Gets a string for the package information sidebar
    public String getCurrentInfo() {
        if (isContentBroken) {
            return "Luokka " + clas + ", " + contained.getName() + ", rikki" + ",\n" + location;
        }
        else return "Luokka " + clas + ", " + contained.getName() + ", ehjä" + ",\n" + location;
    }
}


/*class firstClass extends Package {
    public firstClass(int pID, int mID, int dID, int s, boolean brks, String loc, Item i) {
        packageID = pID;
        machineID = mID;
        destinationID = dID;
        location = loc;
        speed = s;
        breaks = brks;
        contained = i;
    };
}

class secondClass extends Package {
    public secondClass(int pID, int mID, int dID, int s, boolean brks, String loc, Item i) {
        packageID = pID;
        machineID = mID;
        destinationID = dID;
        location = loc;
        speed = s;
        breaks = brks;
        contained = i;
    };
}

class thirdClass extends Package {
    public thirdClass(int pID, int mID, int dID, int s, boolean brks, String loc, Item i) {
        packageID = pID;
        machineID = mID;
        destinationID = dID;
        location = loc;
        speed = s;
        breaks = brks;
        contained = i;
    };
}*/
