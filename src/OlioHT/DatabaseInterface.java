package OlioHT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 * CT60A2411 Olio-ohjelmointi kesäkurssi/CT60A4303 Tietokantojen perusteet kesäkurssi 2018
 * DatabaseInterface.java
 * @author Antti Vilkman 0521281
 * 
 * This class acts as an interface for the database containing the data used by 
 * the application and is used for reading and writing in the database.
 */

public class DatabaseInterface {
    public DatabaseInterface() {}
    
    // Connects to the database
    private Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:HT.db";
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println("Virhe connectissa");
        }
        return conn;
    }
    
    // Creates a package in the database
    public int createPackage(int startID, int destID, int clas, boolean breakable, int distance) {
        int key;
        
        Connection conn = connect();
        String sql = "INSERT INTO Package(MachineID, DestinationID, Class, Breaks, Distance) VALUES (?,?,?,?,?)";
        
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql); 
            pstmt.setInt(1, startID);
            pstmt.setInt(2, destID);
            pstmt.setInt(3, clas);
            pstmt.setBoolean(4, breakable);
            pstmt.setInt(5, distance);
            pstmt.executeUpdate();
            key = pstmt.getGeneratedKeys().getInt(1);
            conn.close();
            return key; 
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
       return 0; 
    }
    
    // Creates a new item in the database
    public void createItem(Item i) {
        Connection conn = connect();
        String sql = "INSERT INTO Item(Name, Weight, Length, Width, Height, Fragile) VALUES (?,?,?,?,?,?)";
        
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql); 
            pstmt.setString(1, i.getName());
            pstmt.setInt(2, i.getWeight());
            pstmt.setInt(3, i.getLength());
            pstmt.setInt(4, i.getWidth());
            pstmt.setInt(5, i.getHeight());
            pstmt.setBoolean(6, i.getFragility());
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    // Puts the an item into a package
    public void itemToPackage(String name, Package p) {
        Connection conn = connect();
        String sql = "INSERT INTO ItemInPackage(PackageID, ItemType) VALUES (?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql); 
            pstmt.setInt(1, p.getID());
            pstmt.setString(2, name);
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    // Gets the existing items from the database and creates an Item object for each one
    public ArrayList getItems() {
        ArrayList<Item> items = new ArrayList();
        Connection conn = connect();
        String sql = "SELECT * FROM Item"; 
        try {
            Statement stmt = conn.createStatement(); 
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                Item i = new Item(rs.getString("Name"), rs.getInt("Weight"), 
                        rs.getInt("Length"), rs.getInt("Width"), rs.getInt("Height"),
                        rs.getBoolean("Fragile"));
                items.add(i);
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return items;
    }
    
    // Finds an item in a package with the given ID and creates an Item object, placing it in the package
    public Item itemFromDb(int pID) {
        Connection conn = connect();
        String sql = "SELECT Item.Name, Item.Weight, Item.Length, Item.Width, Item.Height, Item.Fragile "
                + "FROM Item JOIN ItemInPackage ON Item.Name = ItemInPackage.ItemType "
                + "WHERE ItemInPackage.PackageID = ?"; 
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql); 
            pstmt.setInt(1, pID);
            ResultSet rs = pstmt.executeQuery();
            String name = rs.getString("Name");
            int weight = rs.getInt("Weight"); 
            int length = rs.getInt("Length");
            int width = rs.getInt("Width"); 
            int height = rs.getInt("Height");
            boolean fragile = rs.getBoolean("Fragile");
            conn.close();
            Item i = new Item(name, weight, length, width, height, fragile);
            return i;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    /* Gets a list of existing packages from the database, creating 
    Package objects for each one and returns them in a list */
    public PackageStorage packagesFromDb() {
        PackageStorage storage = new PackageStorage();
        ArrayList<Package> l = new ArrayList();
        
        String sql = "SELECT Package.PackageID, Package.MachineID, "
                + "Package.DestinationID, Package.Class, Package.Breaks, "
                + "Machine.City, Machine.Postoffice"
                + " FROM Package JOIN Machine ON Package.MachineID = Machine.MachineID";
        Connection conn = connect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                int packageID = rs.getInt("PackageID");
                int machineID = rs.getInt("MachineID");
                int destinationID = rs.getInt("DestinationID");
                int clas = rs.getInt("Class");
                boolean breaks = rs.getBoolean("Breaks");
                String loc = rs.getString("Postoffice") + ", " + rs.getString("City");
                Package p = new Package(packageID, machineID, destinationID, clas, breaks, loc, itemFromDb(packageID));
                l.add(p);
            }
            conn.close();
            storage.setList(l);
            return storage;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public void deletePackage(Package p) {
        Connection conn = connect();
        String sql = "DELETE FROM Package WHERE PackageID = ?";
        try {
            conn.createStatement().execute("PRAGMA foreign_keys = ON");
            PreparedStatement pstmt = conn.prepareStatement(sql); 
            pstmt.setInt(1, p.getID());
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
