package OlioHT;

import java.util.ArrayList;

/**
 * CT60A2411 Olio-ohjelmointi kesäkurssi/CT60A4303 Tietokantojen perusteet kesäkurssi 2018
 * Smartpost.java
 * @author Antti Vilkman 0521281
 * 
 * This class handles the information for individual smartpost machines
 */

public class SmartPost {
    
    private int machineID;
    private String code;
    private String city;
    private String address;
    private String postoffice;
    private String availability;
    private locData location;
    
    public SmartPost(int mID, String cod, String cit, String addrss, String office, String availab, String lat, String lng) {
        machineID = mID;
        code = cod;
        city = cit;
        address = addrss;
        postoffice = office;
        availability = availab;
        location = new locData(lat, lng);
    }
    
    public int getID() {
        return machineID;
    }
    
    public ArrayList getLocData() {
        return location.geo;
    }
    
    @Override
    public String toString() {
        return postoffice;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getFullAddress() {
        return address + ", " + code + " " + city;
    }
    
    public String getFullOffice() {
        return postoffice + ", " + city;
    }
    
    public String getInfo() {
        return postoffice + " " + availability;
    }
    
    private class locData {
        ArrayList<String> geo;
        
        public locData(String lat, String lng) {
            geo = new ArrayList();
            geo.add(lat);
            geo.add(lng);
        }
    }
 
}


