package OlioHT;

import java.util.ArrayList;

/**
 * CT60A2411 Olio-ohjelmointi kesäkurssi/CT60A4303 Tietokantojen perusteet kesäkurssi 2018
 * PackageStorage.java
 * @author Antti Vilkman 0521281
 * 
 * This class contains all of the existing packages in a list.
 */

public class PackageStorage {
    private ArrayList<Package> packages;
    
    public PackageStorage() {}
    
    public void setList(ArrayList l) {
        packages = l;
    }
    
    public ArrayList getPackages() {
        return packages;
    }
    
    public void addPackage(Package p) {
        packages.add(p);
    }
    
    public void removePackage(Package p) {
        packages.remove(p);
    }
}
