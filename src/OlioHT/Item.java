package OlioHT;

/**
 *
 * @author Ana
 */

public class Item {
    private String name;
    private int weight;
    private int length; 
    private int width;
    private int height; 
    private boolean isFragile;
    String fragileInfo;
    
    public Item(String n, int wght, int lngth, int wdth, int hght, boolean isFrgl) {
        name = n;
        weight = wght;
        length = lngth;
        width = wdth;
        height = hght;
        isFragile = isFrgl;
        if (isFragile) {
            fragileInfo = ", Särkyvä";
        }
        else fragileInfo = "";
    }
    
    @Override
    public String toString() {
        return name + ", " + weight + "kg, " + length + "cm x " + width + "cm x " + height + "cm" + fragileInfo;
    }
    
    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;  
    }

    public int getHeight() {
        return height;
    }

    public boolean getFragility() {
        return isFragile;
    }
}
