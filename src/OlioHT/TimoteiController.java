/**
 * CT60A2411 Olio-ohjelmointi kesäkurssi/CT60A4303 Tietokantojen perusteet kesäkurssi 2018
 * TimoteiController.java
 * @author Antti Vilkman 0521281
 * 
 * FXML Controller for the application
 */

package OlioHT;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;

public class TimoteiController implements Initializable {
    
    @FXML
    private WebView web;
    @FXML
    private ComboBox<Item> itemCombo;
    @FXML
    private Button placeMachineButton;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemWeightField;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private Pane itemCreation;
    @FXML
    private Button itemMinimizer;
    @FXML
    private ComboBox<String> startingCityCombo;
    @FXML
    private ComboBox<SmartPost> startingMachineCombo;
    @FXML
    private ComboBox<String> destinationCityCombo;
    @FXML
    private ComboBox<SmartPost> destinationMachineCombo;
    @FXML
    private Button createItemButton;
    @FXML
    private Button dbCreator;
    private XMLReader reader;
    private DatabaseInterface dbInterface;
    private PackageStorage storage;
    private SmartPostList smartPosts;
    @FXML
    private GridPane itemGrid;
    @FXML
    private ComboBox<Integer> classCombo;
    @FXML
    private TextField itemLengthField;
    @FXML
    private TextField itemWidthField;
    @FXML
    private TextField itemHeightField;
    @FXML
    private ComboBox<Package> packageChoice;
    @FXML
    private Button sendPackageButton;
    @FXML
    private ComboBox<String> cityCombo;
    @FXML
    private Button routeEraser;
    @FXML
    private GridPane packageDisplay;
    @FXML
    private Button hidePackages;
    @FXML
    private Button showPackages;
    @FXML
    private Button showCreation;
    @FXML
    private ListView<Package> packageList;
    @FXML
    private Button itemCreateButton;
    @FXML
    private Label errorDisp;
    @FXML
    private Label errorDisp2;
    @FXML
    private Label sendingErrDisp;
    @FXML
    private Button constraintButton;
    @FXML
    private TextArea constraintInfo;
    @FXML
    private Button deletePackageButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {    
        dbInterface = new DatabaseInterface();
        ArrayList a = new ArrayList();
        a.add(1);
        a.add(2);
        a.add(3);
        classCombo.setItems(FXCollections.observableArrayList(a));
        smartPosts = new SmartPostList();
        storage = dbInterface.packagesFromDb();
        itemCombo.setItems(FXCollections.observableArrayList(dbInterface.getItems()));
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        // Text on map was turning into gibberish on my mac, line below fixes it.
        web.getEngine().setUserAgent(web.getEngine().getUserAgent().replace("Macintosh; ", ""));
        updatePackageList();
    }    

    // Hides the item creation sidebar
    @FXML
    private void hideItemCreation(ActionEvent event) {
        itemGrid.setVisible(false);
        showCreation.setVisible(true);
    }

    // Un-hides the item creation sidebar
    @FXML
    private void showItemCreation(ActionEvent event) {
        itemGrid.setVisible(true);
        showCreation.setVisible(false);
    }
    
    // Creates a new item
    @FXML
    private void createItem(ActionEvent event) {
        errorDisp.setText("");
        try {
            String name = itemNameField.getText();
            if (name.equals("")) {
                errorDisp.setText("Anna esineelle nimi!");
                return;
            }
            int weight = Integer.parseInt(itemWeightField.getText());
            int length = Integer.parseInt(itemLengthField.getText());
            int width = Integer.parseInt(itemWidthField.getText());
            int height = Integer.parseInt(itemHeightField.getText()); 
            boolean isFragile = fragileCheck.isSelected();

            Item i = new Item(name, weight, length, width, height, isFragile);
            itemCombo.getItems().add(i);
            dbInterface.createItem(i);
            errorDisp.setText("Esine luotu.");
            clearItemInputs();
        } catch (NumberFormatException e) {
            errorDisp.setText("Syötä mitoille ja painolle\nkokonaislukuja!");            
        }
    }

    /* Checks if given item fits into the selected package type and creates the 
    package if everything is ok */
    @FXML
    private void createPackage(ActionEvent event) {
        errorDisp2.setText("");
        try {
            // Starting and destination points
            SmartPost strtMachine = startingMachineCombo.getValue();
            SmartPost destMachine = destinationMachineCombo.getValue();

            // Class, fragility and location of the package
            int c = classCombo.getValue();
            boolean breaks;
            String loc = strtMachine.getFullAddress();

            // Gets the geographical starting and ending points for the route as well as distance
            ArrayList route = strtMachine.getLocData();
            route.addAll(destMachine.getLocData());
            double distance = (double) web.getEngine().executeScript("document.getDistance("+route+")");

            // Item that we will try to package
            Item i = itemCombo.getValue();
            boolean isFragile = i.getFragility();
            breaks = (c != 2 && isFragile);

            // Checks the constraints
            if (checkConstraints(i, distance, c)) {
                int key = dbInterface.createPackage(strtMachine.getID(), destMachine.getID(), c, breaks, (int)distance);
                Package p = new Package(key, strtMachine.getID(), destMachine.getID(), c, breaks, loc, i);
                dbInterface.itemToPackage(i.getName(), p);
                errorDisp2.setText("Paketti luotu.");
                clearPackageInputs();
            } else errorDisp2.setText("Esine ei mahdu tai matka\non liian pitkä!");
            storage = dbInterface.packagesFromDb();
            updatePackageList();
        } catch (NullPointerException e) {
            errorDisp2.setText("Anna esine, pakettiluokka\nlähtöpaikka sekä määränpää!");
        }
    }

    // Fetches the machine data from an XML file and inserts it into the database
    @FXML
    private void machinesToDb(ActionEvent event) {
        reader = new XMLReader();
        reader.smartPostToDatabase2();
        smartPosts = new SmartPostList();
    }
    
    // Fetches cities into the city selection combobox for placing machines onto the map
    @FXML
    private void updateCities(MouseEvent event) {
        cityCombo.setItems(FXCollections.observableArrayList(smartPosts.getDistinctCities()));
    }

    // Places machines into the selected area
    @FXML
    private void placeMachines(ActionEvent event) { 
        for(SmartPost m : (ArrayList<SmartPost>) (smartPosts.getMachinesInArea(cityCombo.getValue()))) {
            String addr = m.getFullAddress();
            String info = m.getInfo();
            web.getEngine().executeScript("document.goToLocation('"+addr+"', '"+info+"', 'red')");
        }
        cityCombo.setValue(null);
    }

    // Gets the list of cities to send a package from
    @FXML
    private void getStartingCities(MouseEvent event) {
        startingCityCombo.setItems(FXCollections.observableArrayList(smartPosts.getDistinctCities()));
    }

    // Gets the list of machines to send a package from
    @FXML
    private void getStartingOffices(MouseEvent event) {
        startingMachineCombo.setItems(FXCollections.observableArrayList(
                smartPosts.getMachinesInArea(startingCityCombo.getValue()))
        );
    }

    // Gets the list of cities to send a package to
    @FXML
    private void getDestCities(MouseEvent event) {
        destinationCityCombo.setItems(FXCollections.observableArrayList(smartPosts.getDistinctCities()));
    }

    // Gets the list of machines to send a package to
    @FXML
    private void getDestOffices(MouseEvent event) {
        destinationMachineCombo.setItems(FXCollections.observableArrayList(
                smartPosts.getMachinesInArea(destinationCityCombo.getValue()))
        );

    }    

    // Gets a list of existing packages
    @FXML
    private void getPackages(MouseEvent event) {
        packageChoice.setItems(FXCollections.observableArrayList(storage.getPackages()));
    }

    // Sends a package, drawing the route on the map
    @FXML
    private void sendPackage(ActionEvent event) {
        try {
            Package p = packageChoice.getValue();
            sendingErrDisp.setText("");
            if (p.getMacID() == p.getDestID()) {
                sendingErrDisp.setText("Paketti on jo perillä!");
            } 
            else {
                ArrayList route = smartPosts.findMachine(p.getMacID()).getLocData();
                route.addAll(smartPosts.findMachine(p.getDestID()).getLocData());
                int speed = 4 - p.getClas();
                web.getEngine().executeScript("document.createPath("+route+", 'red', '"+speed+"')");
                p.send(smartPosts.findMachine(p.getDestID()).getFullOffice());
                
                updatePackageList();
            }
        } catch (NullPointerException e) {}
    }

    // Erases all routes from the map
    @FXML
    private void eraseRoutes(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }
    
    // Hides the sidebar containing package info
    @FXML
    private void hidePackageDisplay(ActionEvent event) {
        packageDisplay.setVisible(false);
        showPackages.setVisible(true);
    }

    // Shows the sidebar containing package info
    @FXML
    private void showPackageDisplay(ActionEvent event) {
        packageDisplay.setVisible(true);
        showPackages.setVisible(false);
    }
    
    // Updates the information in the sidebar containing package info
    private void updatePackageList() {
        ArrayList infos = new ArrayList();
        infos.add(null);
        for (Package p : (ArrayList<Package>)storage.getPackages()) {
            infos.add(p.getCurrentInfo());
        }
        packageList.setItems(FXCollections.observableArrayList(infos));
    }
    
    // Clears relevant input fields after an item is created
    private void clearItemInputs() {
        itemNameField.clear();
        itemWeightField.clear();
        itemLengthField.clear();
        itemWidthField.clear();
        itemHeightField.clear();
        fragileCheck.setSelected(false);
    }
    
    // Clears relevant input fields after a package is created
    private void clearPackageInputs() {
        itemCombo.setValue(null);
        startingCityCombo.setValue(null);
        startingMachineCombo.setValue(null);
        destinationCityCombo.setValue(null);
        destinationMachineCombo.setValue(null);
        classCombo.setValue(null);
    }

    @FXML
    private void toggleConstraints(ActionEvent event) {
        if (constraintInfo.isVisible()) {
            constraintInfo.setVisible(false);
        }
        else {
            constraintInfo.setVisible(true);
        }
    }

    @FXML
    private void deletePackage(ActionEvent event) {
        try {
            Package p = packageChoice.getValue();
            dbInterface.deletePackage(p);
            storage.removePackage(p);
            updatePackageList();
            packageChoice.setItems(FXCollections.observableArrayList(storage.getPackages()));
            sendingErrDisp.setText("");
        } catch (NullPointerException e) {}
    }
    
    // Checks whether given dimensions for an item will fit into the given type of package
    public boolean checkConstraints(Item i, double dist, int c) {
        
        switch (c) {
            case 1:
                return dist <= 150;
            case 2:
                return i.getLength() <= 100 && i.getWidth() <=100 && i.getHeight() <= 100;
            case 3:
                return true;
            default:
                return false; 
        }
    }
}
