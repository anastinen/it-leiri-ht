package OlioHT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * CT60A2411 Olio-ohjelmointi kesäkurssi/CT60A4303 Tietokantojen perusteet kesäkurssi 2018
 * SmartPostList.java
 * @author Antti Vilkman 0521281
 * 
 * This class contains all of the smartpost machines in a list.
 */

public class SmartPostList {
    private ArrayList<SmartPost> machines;
    
    public SmartPostList() {
        machines = new ArrayList();
        getMachinesFromDb();
    }
   
    // Fetches smartpost machines from the database and puts them to a list 
    private void getMachinesFromDb() {
        try {
            String url = "jdbc:sqlite:HT.db";
            Connection conn = DriverManager.getConnection(url);
            String sql = "SELECT * FROM Machine";
            Statement stmt = conn.createStatement(); 
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                int mID = rs.getInt("MachineID");
                String code = rs.getString("Code");
                String city = rs.getString("City");
                String address = rs.getString("Address");
                String office = rs.getString("Postoffice");
                String avail = rs.getString("Availability");
                String lat = rs.getString("Latitude");
                String lng = rs.getString("Longitude");
                SmartPost s = new SmartPost(mID, code, city, address, office, avail, lat, lng);
                machines.add(s);
            }
            conn.close();
        } catch (SQLException e) {
            System.out.println("Virhe connectissa");
        }
    }
    
    public ArrayList<String> getDistinctCities() {
        ArrayList<String> cities = new ArrayList();
        for (SmartPost m : machines) {
            if (!cities.contains(m.getCity())) {
                cities.add(m.getCity());
            }
        }
        return cities;
    }
    
    // Gets machines in a given area
    public ArrayList getMachinesInArea(String area) {
        ArrayList<SmartPost> a = new ArrayList();
        for(SmartPost s : machines) {
            if (s.getCity().equals(area)) {
                a.add(s);
            }
        }
        return a;
    }
    
    // Finds machine with the given id
    public SmartPost findMachine(int id) {
        for (SmartPost s : machines) {
            if (s.getID() == id) {
                return s;
            }
        }
        return null;
    }
}
