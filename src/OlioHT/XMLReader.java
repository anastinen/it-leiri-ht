package OlioHT;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CT60A2411 Olio-ohjelmointi kesäkurssi/CT60A4303 Tietokantojen perusteet kesäkurssi 2018
 * XMLReader.java
 * @author Antti Vilkman 0521281
 * 
 * Reads the XML file containing machine data and inserts it into the database
 */

public class XMLReader {
    private Document doc;
    
    public XMLReader() {
        //parseCurrentData(getData("http://www.smartpost.ee/fi_apt.xml"));
        parseCurrentData(getData("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT"));
    }
    
    public String getData(String u) {
        String total = "";
        try {
            URL url;
            url = new URL(u);
      
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String inputLine;
            while((inputLine = in.readLine()) != null) {
                total += inputLine + "\n";
            }
        in.close();
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException, getData!");
        } catch (IOException ex) {
            System.out.println("IOException, getData!");
        }
        return total;
    }
    
    private void parseCurrentData(String total) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(total)));
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
        }
    }
    
    public void smartPostToDatabase2() {
        String address, code, city, postoffice, availability, lat, lng;
        
        Connection conn = connect();
        try {
            conn.createStatement().execute("DELETE FROM Machine");
        } catch (SQLException e) {}
        NodeList nodes = doc.getElementsByTagName("item");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            code = getValue("postalcode", e);
            address = getValue("address", e);
            city = getValue("city", e);
            postoffice = getValue("name", e);
            availability = getValue("availability", e);
            lat = getValue("lat", e);
            lng = getValue("lng", e);
            
            String sql = "INSERT INTO Machine(Code, City, Address, "
                    + "Postoffice, Availability, Latitude, Longitude) VALUES(?,?,?,?,?,?,?)";
            
            try {
                PreparedStatement pstmt = conn.prepareStatement(sql); 
                pstmt.setString(1, code);
                pstmt.setString(2, city);
                pstmt.setString(3, address);
                pstmt.setString(4, postoffice);
                pstmt.setString(5, availability);
                pstmt.setString(6, lat);
                pstmt.setString(7, lng);
                pstmt.executeUpdate();
            } catch (SQLException ex) {
                //System.out.println("Virhe automaatin insertissä!");
                System.out.println(ex.getMessage());
            }
        }
        try {
            conn.close();
            } catch (SQLException ex) {
            System.out.println("conn.closessa ongelma");
        }
    }
        
    private String getValue(String tag, Element e) {
        return ((Element) (e.getElementsByTagName(tag).item(0))).getTextContent();
    }
    
    private Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:HT.db";
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println("Virhe connectissa");
        }
        return conn;
    }
}

